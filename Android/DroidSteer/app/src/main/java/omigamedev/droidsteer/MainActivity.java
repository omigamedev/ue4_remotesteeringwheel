package omigamedev.droidsteer;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Xml;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.Socket;


public class MainActivity extends ActionBarActivity implements SensorEventListener {
    private SensorManager sm;
    private Sensor rotationSensor;
    float[] rotMat = new float[9];
    float yaw;
    float pitch;
    float roll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        sm = (SensorManager)getSystemService(SENSOR_SERVICE);
        rotationSensor = sm.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);

        rotMat[ 0] = 1;
        rotMat[ 4] = 1;
        rotMat[ 8] = 1;

        try {
            final DatagramSocket socket = new DatagramSocket();
            socket.setBroadcast(true);
            final InetAddress address = getBroadcastAddress();// InetAddress.getByName("192.168.127.150");
            Thread t = new Thread(){
                @Override
                public void run() {
                    super.run();
                    try {
                        while (true) {
                            String message = String.format("%f|%f|%f$", pitch, roll, yaw);
                            DatagramPacket packet = new DatagramPacket(message.getBytes(), message.length(), address, 5555);
                            socket.send(packet);
                            Thread.sleep(1000/120);
                        }
                    } catch (IOException | InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            t.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    InetAddress getBroadcastAddress() throws IOException {
        WifiManager wifi = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcp = wifi.getDhcpInfo();
        // handle null somehow

        int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
            quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
        return InetAddress.getByAddress(quads);
    }

    @Override
    protected void onStart() {
        super.onStart();
        sm.registerListener(this, rotationSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sm.unregisterListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float[] vals = new float[3];
        if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            SensorManager.getRotationMatrixFromVector(rotMat, event.values);
            SensorManager.remapCoordinateSystem(rotMat, SensorManager.AXIS_X, SensorManager.AXIS_Y, rotMat);
            SensorManager.getOrientation(rotMat, vals);
            yaw = (float)Math.toDegrees(vals[0]); // in degrees [-180, +180]
            pitch = (float)Math.toDegrees(vals[1]);
            roll = (float)Math.toDegrees(vals[2]);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
