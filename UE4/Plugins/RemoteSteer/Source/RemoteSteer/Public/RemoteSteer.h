#pragma once

#include "ModuleManager.h"
#include "UdpSocketReceiver.h"
#include "RemoteSteer.generated.h"

UCLASS()
class URemoteSteerBPFL : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category = RemoteSteer)
	static FRotator GetSteerRotation();
};

class FRemoteSteerModule : public IModuleInterface
{
public:
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	void OnData(const FArrayReaderPtr& ar, const FIPv4Endpoint& sender);
};