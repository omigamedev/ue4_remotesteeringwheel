#include "RemoteSteerPrivatePCH.h"

#define LOCTEXT_NAMESPACE "FRemoteSteerModule"

FRotator CurrentRotation;

FRotator URemoteSteerBPFL::GetSteerRotation()
{
	return CurrentRotation;
}

void FRemoteSteerModule::StartupModule()
{
	FSocket* Socket = FUdpSocketBuilder(TEXT("UdpListener"))
		.AsReusable()
		.BoundToPort(5555)
		.Build();
	FUdpSocketReceiver* receiver = new FUdpSocketReceiver(Socket, FTimespan(), TEXT("UdpListenerThread"));
	receiver->OnDataReceived().BindRaw(this, &FRemoteSteerModule::OnData);
}

void FRemoteSteerModule::ShutdownModule()
{
}

void FRemoteSteerModule::OnData(const FArrayReaderPtr& ar, const FIPv4Endpoint& sender)
{
	char* Data = (char*)ar->GetData();
	int i = 0;
	for (; i < 64; i++)
		if (Data[i] == '$') break;
	Data[i] = '\0';
	FString Msg = FString(ANSI_TO_TCHAR(Data));
	TArray<FString> parts;
	Msg.ParseIntoArray(parts, TEXT("|"), true);
	float pitch = FCString::Atof(*parts[0]);
	float roll = FCString::Atof(*parts[1]);
	float yaw = FCString::Atof(*parts[2]);
	CurrentRotation = FRotator(pitch, yaw, roll);
	LOG("%s", *Msg);
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FRemoteSteerModule, RemoteSteer)
DEFINE_LOG_CATEGORY(LogRemoteSteer);
