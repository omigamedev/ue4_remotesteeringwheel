#pragma once

#include "Engine.h"
#include "Networking.h"
#include "Sockets.h"
#include "UdpSocketBuilder.h"
#include "UdpSocketReceiver.h"
#include "RemoteSteer.h"
#include "Kismet/BlueprintFunctionLibrary.h"

DECLARE_LOG_CATEGORY_EXTERN(LogRemoteSteer, Log, All);
#define LOG(M,...) UE_LOG(LogRemoteSteer, Warning, TEXT(M), ##__VA_ARGS__); \
	if(GEngine && UE_BUILD_DEBUG) GEngine->AddOnScreenDebugMessage(-1, 3, FColor::Red, FString::Printf(TEXT(M), ##__VA_ARGS__));
