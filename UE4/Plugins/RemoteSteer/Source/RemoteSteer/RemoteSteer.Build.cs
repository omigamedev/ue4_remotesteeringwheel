using UnrealBuildTool;

public class RemoteSteer : ModuleRules
{
	public RemoteSteer(TargetInfo Target)
	{
		PublicIncludePaths.AddRange(new string[] { "RemoteSteer/Public" });
		PrivateIncludePaths.AddRange(new string[] { "RemoteSteer/Private" });

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "Networking", "Sockets" });
		PrivateDependencyModuleNames.AddRange(new string[]{ "Slate", "SlateCore" });
		DynamicallyLoadedModuleNames.AddRange(new string[]{});
	}
}
